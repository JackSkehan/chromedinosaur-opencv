# Description:
We are going to use Python and OpenCV to play the google chrome dinosaur game. 

Here I’m using mac, and my webcam for the camera feed

This program gives us the ability to play the Chrome Dinosaur game using hand movements from the camera feed.

# Run Using:
Run with python3 using 'python3 main.py'


# Credits
created following https://levelup.gitconnected.com/playing-chromes-dinosaur-game-using-opencv-19b3cf9c3636



# Modifications
reversed image frame so video output acts as a mirror rather than.

also modified code to work for mac.

